import wx
import socket
import thread
import time
import threading
import winsound

class ClientFrame(wx.Frame):
	def __init__(self):
		self.connected = False
		self.version = "v_0.4"
		self.createFrame()

	def createFrame(self):
		style = (wx.NO_BORDER)
		wx.Frame.__init__(self, None, title='Client', style = style)
		self.panel = wx.Panel(self)
		self.SetSize((400, 500))
		self.panel.SetSize((400, 500))
		self.t = wx.TextCtrl(self.panel, id=0, value="", pos=(0,375),
		size=(400,100), style = wx.TE_MULTILINE | wx.TE_PROCESS_ENTER, validator=wx.DefaultValidator,
		name=wx.TextCtrlNameStr)
		self.panel.SetBackgroundColour("white")
		
		self.c = wx.TextCtrl(self.panel, id=0, value="", pos=(0,100),
		size=(400,275), style = wx.TE_MULTILINE | wx.TE_READONLY, validator=wx.DefaultValidator,
		name=wx.TextCtrlNameStr)
		
		self.nameLabel = wx.StaticText(self.panel, label = "Name:", pos = (0, 0), size = (40,20))
		self.nameSetter = wx.TextCtrl(self.panel, id=0, value="", pos=(40,0), size=(100,20), validator=wx.DefaultValidator, name=wx.TextCtrlNameStr)
		
		self.hostLabel = wx.StaticText(self.panel, label = "Host:", pos = (0, 20), size = (40,20))
		self.hostSetter = wx.TextCtrl(self.panel, id=0, value="", pos=(40,20), size=(100,20), validator=wx.DefaultValidator, name=wx.TextCtrlNameStr)
		 
		self.portLabel = wx.StaticText(self.panel, label = "Port:", pos = (0, 40), size = (40,20))
		self.portSetter = wx.TextCtrl(self.panel, id=0, value="", pos=(40,40), size=(100,20), validator=wx.DefaultValidator, name=wx.TextCtrlNameStr)
		
		self.label = wx.StaticText(self.panel, label = "Chat History", pos = (0, 80), size = (100,20))
		self.send = wx.Button(self.panel, label="Send", pos = (0,475), size = (400, 25))
		self.send.Bind(wx.EVT_BUTTON, self.OnSend)
		
		self.button = wx.Button(self.panel, label="Connect", pos = (150,0), size = (100,20))
		self.button.Bind(wx.EVT_BUTTON, self.OnConnect)
		
		self.disconnect = wx.Button(self.panel, label="Disconnect", pos = (150,20), size = (100,20))
		self.disconnect.Bind(wx.EVT_BUTTON, self.OnDisconnect)
		
		self.close = wx.Button(self.panel, label="Close", pos = (150,40), size = (100,20))
		self.close.Bind(wx.EVT_BUTTON, self.OnClose)
		
		self.Bind(wx.EVT_MOTION, self.OnMouse)
		self.panel.Bind(wx.EVT_MOTION, self.OnMouse)
		self.t.Bind(wx.EVT_TEXT_ENTER, self.OnSend)
		self.Show(True)
		
	def OnConnect(self, event):
		if not self.connected:
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.sock.connect((self.hostSetter.GetLineText(0),int(self.portSetter.GetLineText(0))))
			thread.start_new_thread(self.connectionListener, ())
			self.c.AppendText("Connected to server! \n")
			self.sock.send("NAMESET:" + self.nameSetter.GetLineText(0))
			self.sock.send("VERSION:"+self.version)
			self.connected = True
	def OnDisconnect(self, event):
		self.sock.send("DISCONNECT")
		
	def OnMouse(self, event):
		if not event.Dragging():
			self._dragPos = None
			return
		if not self._dragPos:
			self._dragPos = event.GetPosition()
		else:
			pos = event.GetPosition()
			displacement = self._dragPos - pos
			self.SetPosition( self.GetPosition() - displacement)
	def OnClose(self, event):
		self.Close(True)
	def OnSend(self, event):
		for num in xrange(0, self.t.GetNumberOfLines()):
			if not self.t.GetLineText(num) == "":
				self.sock.send(self.nameSetter.GetLineText(0) + " : " + self.t.GetLineText(num) + "")
				self.c.AppendText("You : "+ self.t.GetLineText(num) + "\n")
		self.t.Clear()
		self.t.SetValue("")
	def connectionListener(self):
		while True:
			try:
				data = self.sock.recv(2048)
				if data == "SERVERPING":
					pass
				elif data == "DISCONNECTACCEPTED":
					self.sock.close()
					self.c.AppendText("Disconnected from server! \n")
					self.connected = False
					break
				elif data == "SYS-REMOTESHUTDOWN":
					self.sock.close()
					self.c.AppendText("Forced disconnect from server! \n")
					self.connected = False
					break
				else:
					self.c.AppendText(data + "\n")
					winsound.PlaySound("SystemExclamation", winsound.SND_ALIAS | winsound.SND_ASYNC)
			except:
				self.sock.close()
				self.c.AppendText("Forced disconnect from server! \n")
				self.connected = False
				break
app = wx.App()
f = ClientFrame()
app.MainLoop()